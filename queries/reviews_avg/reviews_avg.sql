WITH
games_companies AS (
    SELECT 
    DISTINCT ON (company_id, game_id)
        company_id,
        game_id 
    FROM (
        SELECT company_id, game_id
        FROM developers
        UNION
        SELECT company_id, game_id
        FROM publishers
    ) as union_result
),
avg_game AS (
    SELECT
        game_id,
        AVG(recommended::INTEGER) as avg_mark
    FROM reviews
    WHERE CURRENT_DATE - reviews.posted <= 30
    GROUP BY game_id
),
avg_company AS (
    SELECT
        games_companies.company_id,
        AVG(avg_game.avg_mark) as avg_mark
    FROM (
        games_companies
        JOIN
        avg_game ON games_companies.game_id = avg_game.game_id
    )
    GROUP BY games_companies.company_id
)
SELECT
    companies.title,
    avg_company.avg_mark
FROM (
    companies
    JOIN
    avg_company ON companies.company_id = avg_company.company_id
)
ORDER BY avg_mark DESC
 
