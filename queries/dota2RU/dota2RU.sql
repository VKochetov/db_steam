WITH 
dota2_id AS (
    SELECT game_id
    FROM games
    WHERE games.title = 'DOTA 2'
),
dota2_players AS (
    SELECT login, in_game_time
    FROM libraries, dota2_id
    WHERE libraries.game_id = dota2_id.game_id
),
dota2_players_RU AS (
    SELECT join_result.login, join_result.country, join_result.in_game_time
    FROM (
        SELECT users.login, users.country, dota2_players.in_game_time
        FROM (
            users
            JOIN
            dota2_players ON users.login = dota2_players.login
        )
    ) as join_result
    WHERE join_result.country = 'Russian Federation'
)
SELECT SUM(in_game_time) as total_time
FROM dota2_players_RU

