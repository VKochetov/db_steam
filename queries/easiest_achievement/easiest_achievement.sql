WITH
games_achievements AS (
    SELECT
        achievements_users.achievement_id,
        achievements.game_id,
        achievements.title
    FROM (
        achievements_users
        JOIN
        achievements ON achievements_users.achievement_id = achievements.achievement_id
    )
),
count_achievements AS (
    SELECT
        game_id,
        achievement_id,
        title,
        COUNT(achievement_id) as count_a
    FROM games_achievements
    GROUP BY achievement_id, game_id, title
),
count_games AS (
    SELECT game_id, COUNT(game_id) AS count_g
    FROM libraries 
    GROUP BY game_id
)
SELECT
    count_achievements.title,
    count_achievements.count_a::DECIMAL / count_games.count_g::DECIMAL AS ratio
FROM (
    count_achievements
    JOIN
    count_games ON count_achievements.game_id = count_games.game_id
)
ORDER BY ratio DESC
LIMIT 1

