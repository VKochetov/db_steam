WITH
all_commentators AS (
    SELECT sender_login as login
    FROM comments_on_users
    UNION
    SELECT login
    FROM comments_on_reviews
    UNION
    SELECT login
    FROM comments_on_news
),
all_commentators_countries AS (
    SELECT join_result.login, join_result.country
    FROM (
        SELECT all_commentators.login, users.country
        FROM (
            all_commentators
            JOIN
            users ON all_commentators.login = users.login
        )
    ) as join_result
)
SELECT top_county.country
FROM (
    SELECT 
        all_commentators_countries.country,
        COUNT(all_commentators_countries.country) as comments
    FROM all_commentators_countries 
    GROUP BY all_commentators_countries.country
    ORDER BY comments DESC
    LIMIT 1
) AS top_county

