SELECT 
    games.title AS game, 
    games_copies.copies * games.price AS sales
FROM games
NATURAL INNER JOIN (
    SELECT game_id, COUNT(game_id) AS copies
    FROM libraries 
    GROUP BY game_id
) AS games_copies 
ORDER BY sales DESC;
