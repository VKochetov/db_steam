CREATE OR REPLACE FUNCTION get_game_by_title(title TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        id UUID;
    BEGIN
        SELECT game_id 
        FROM games 
        WHERE get_game_by_title.title = games.title
        INTO id;

        RETURN id;
    END;
    $$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_company_by_title(title TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        id UUID;
    BEGIN
        SELECT company_id 
        FROM companies 
        WHERE get_company_by_title.title = companies.title
        INTO id;

        RETURN id;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Counter-Strike'),
    get_company_by_title('Valve');
    
INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Counter-Strike: Source'),
    get_company_by_title('Valve');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Counter-Strike: Global Offensive'),
    get_company_by_title('Valve');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Fallout: A Post Nuclear Role Playing Game'),
    get_company_by_title('Bethesda Softworks');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Fallout 2: A Post Nuclear Role Playing Game'),
    get_company_by_title('Bethesda Softworks');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Fallout: New Vegas'),
    get_company_by_title('Bethesda Softworks');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Forza Horizon 4'),
    get_company_by_title('Xbox Game Studios');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Forza Horizon 5'),
    get_company_by_title('Xbox Game Studios');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Grand Theft Auto IV'),
    get_company_by_title('Rockstar Games');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Grand Theft Auto V'),
    get_company_by_title('Rockstar Games');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('DOTA 2'),
    get_company_by_title('Valve');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Red Dead Redemption 2'),
    get_company_by_title('Rockstar Games');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Cyberpunk 2077'),
    get_company_by_title('CD PROJEKT RED');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('RUST'),
    get_company_by_title('Facepunch Studios');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('PUBG: BATTLEGROUNDS'),
    get_company_by_title('KRAFTON, Inc.');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Dead by Daylight'),
    get_company_by_title('Behaviour Interactive Inc.');
    
INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('FIFA 22'),
    get_company_by_title('Electronics Arts');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Hunt: Showdown'),
    get_company_by_title('Crytek');

INSERT INTO publishers
(game_id, company_id)
SELECT
    get_game_by_title('Tom Clancy''s Rainbow Six Siege'),
    get_company_by_title('Ubisoft');

CREATE OR REPLACE FUNCTION get_game_by_title(title TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        id UUID;
    BEGIN
        SELECT game_id 
        FROM games 
        WHERE get_game_by_title.title = games.title
        INTO id;

        RETURN id;
    END;
    $$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_company_by_title(title TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        id UUID;
    BEGIN
        SELECT company_id 
        FROM companies 
        WHERE get_company_by_title.title = companies.title
        INTO id;

        RETURN id;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Counter-Strike'),
    get_company_by_title('Valve');
    
INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Counter-Strike: Source'),
    get_company_by_title('Valve');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Counter-Strike: Global Offensive'),
    get_company_by_title('Valve');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Fallout: A Post Nuclear Role Playing Game'),
    get_company_by_title('Interplay Inc.');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Fallout 2: A Post Nuclear Role Playing Game'),
    get_company_by_title('Black Isle Studios');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Fallout: New Vegas'),
    get_company_by_title('Obsidian Entertainment');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Forza Horizon 4'),
    get_company_by_title('Playground Games');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Forza Horizon 5'),
    get_company_by_title('Playground Games');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Grand Theft Auto IV'),
    get_company_by_title('Rockstar Games');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Grand Theft Auto V'),
    get_company_by_title('Rockstar Games');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('DOTA 2'),
    get_company_by_title('Valve');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Red Dead Redemption 2'),
    get_company_by_title('Rockstar Games');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Cyberpunk 2077'),
    get_company_by_title('CD PROJEKT RED');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('RUST'),
    get_company_by_title('Facepunch Studios');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('PUBG: BATTLEGROUNDS'),
    get_company_by_title('KRAFTON, Inc.');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Dead by Daylight'),
    get_company_by_title('Behaviour Interactive Inc.');
    
INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('FIFA 22'),
    get_company_by_title('Electronics Arts');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Hunt: Showdown'),
    get_company_by_title('Crytek');

INSERT INTO developers
(game_id, company_id)
SELECT
    get_game_by_title('Tom Clancy''s Rainbow Six Siege'),
    get_company_by_title('Ubisoft');

CREATE OR REPLACE FUNCTION get_game_by_title(title TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        id UUID;
    BEGIN
        SELECT game_id 
        FROM games 
        WHERE get_game_by_title.title = games.title
        INTO id;

        RETURN id;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Counter-Strike'),
    'Action';
    
INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Counter-Strike: Source'),
    'Action';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Counter-Strike: Global Offensive'),
    'Action';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Fallout: A Post Nuclear Role Playing Game'),
    'RPG';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Fallout 2: A Post Nuclear Role Playing Game'),
    'RPG';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Fallout: New Vegas'),
    'RPG';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Forza Horizon 4'),
    'Racing';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Forza Horizon 5'),
    'Racing';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Grand Theft Auto IV'),
    'Adventure';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Grand Theft Auto V'),
    'Adventure';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('DOTA 2'),
    'Strategy';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Red Dead Redemption 2'),
    'Adventure';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Cyberpunk 2077'),
    'RPG';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('RUST'),
    'MMO';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('PUBG: BATTLEGROUNDS'),
    'Action';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Dead by Daylight'),
    'Action';
    
INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('FIFA 22'),
    'Sports';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Hunt: Showdown'),
    'Action';

INSERT INTO genres_games
(game_id, genre)
SELECT
    get_game_by_title('Tom Clancy''s Rainbow Six Siege'),
    'Action';

CREATE OR REPLACE FUNCTION get_game_by_title(title TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        id UUID;
    BEGIN
        SELECT game_id 
        FROM games 
        WHERE get_game_by_title.title = games.title
        INTO id;

        RETURN id;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike'),
    'FPS';
    
INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike'),
    'Competetive';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike'),
    'Classic';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike: Source'),
    'FPS';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike: Source'),
    'Competetive';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike: Source'),
    'Multiplayer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike: Global Offensive'),
    'FPS';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike: Global Offensive'),
    'Competetive';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Counter-Strike: Global Offensive'),
    'Multiplayer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout: A Post Nuclear Role Playing Game'),
    'Classic';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout: A Post Nuclear Role Playing Game'),
    'Post-apocalyptic';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout: A Post Nuclear Role Playing Game'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout 2: A Post Nuclear Role Playing Game'),
    'Classic';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout 2: A Post Nuclear Role Playing Game'),
    'Post-apocalyptic';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout 2: A Post Nuclear Role Playing Game'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout: New Vegas'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Fallout: New Vegas'),
    'Post-apocalyptic';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Forza Horizon 4'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Forza Horizon 4'),
    'Driving';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Forza Horizon 4'),
    'Third-person';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Forza Horizon 5'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Forza Horizon 5'),
    'Driving';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Forza Horizon 5'),
    'Third-person';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Grand Theft Auto IV'),
    'Third-person';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Grand Theft Auto IV'),
    'Crime';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Grand Theft Auto IV'),
    'Story rich';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Grand Theft Auto V'),
    'Third-person';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Grand Theft Auto V'),
    'Crime';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Grand Theft Auto V'),
    'Story rich';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('DOTA 2'),
    'MOBA';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('DOTA 2'),
    'Competetive';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Red Dead Redemption 2'),
    'Story rich';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Red Dead Redemption 2'),
    'Western';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Red Dead Redemption 2'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Cyberpunk 2077'),
    'Open World';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Cyberpunk 2077'),
    'Story rich';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Cyberpunk 2077'),
    'Sci-fi';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('RUST'),
    'Survival';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('RUST'),
    'Crafting';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('RUST'),
    'Multiplayer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('PUBG: BATTLEGROUNDS'),
    'Multiplayer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('PUBG: BATTLEGROUNDS'),
    'Battle Royale';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('PUBG: BATTLEGROUNDS'),
    'Competetive';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Dead by Daylight'),
    'Competetive';
    
INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Dead by Daylight'),
    'Horror';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('FIFA 22'),
    'Soccer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Hunt: Showdown'),
    'Horror';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Hunt: Showdown'),
    'Western';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Hunt: Showdown'),
    'Multiplayer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Tom Clancy''s Rainbow Six Siege'),
    'Multiplayer';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Tom Clancy''s Rainbow Six Siege'),
    'Tactical';

INSERT INTO tags_games
(game_id, tag)
SELECT
    get_game_by_title('Tom Clancy''s Rainbow Six Siege'),
    'Competetive';

CREATE OR REPLACE FUNCTION gen_purchased(login TEXT, game_id UUID) 
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(max_date_temp) FROM (
            SELECT registered AS max_date_temp FROM users WHERE users.login = gen_purchased.login
            UNION
            SELECT released AS max_date_temp FROM games WHERE games.game_id = gen_purchased.game_id
        ) as un into max_date;
        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO libraries 
(game_id, login, in_game_time, bill, purchased)
    SELECT
        arrays.game_id[gen.i % ARRAY_LENGTH(arrays.game_id,1) + 1] AS game_id,
        arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
        (((random() * 180000)::TEXT)||'SEC')::INTERVAL,
        MD5(random()::TEXT),
        gen_purchased(
            arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1], 
            arrays.game_id[gen.i % ARRAY_LENGTH(arrays.game_id,1) + 1]
        )
    FROM (
        SELECT generate_series(1,1000), FLOOR(random()*10000+1)::INTEGER as i
    ) AS gen
    CROSS JOIN(
        SELECT 
            ARRAY (
                SELECT login FROM users
            ) as login,
            ARRAY (
                SELECT game_id FROM games
            ) as game_id
    ) AS arrays
ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_game_not_in_library(login TEXT) 
    RETURNS UUID AS
    $$
    DECLARE
        games_array UUID[];
    BEGIN
        SELECT
        ARRAY(
            SELECT games_for_login.game_id
            FROM (
                SELECT all_games.login, all_games.game_id
                FROM (
                    users
                    CROSS JOIN
                    games
                ) as all_games
                WHERE(
                    (
                        (all_games.login, all_games.game_id)
                    ) NOT IN (
                        SELECT libraries.login, libraries.game_id
                        FROM libraries
                    )
                    AND
                    all_games.login = gen_game_not_in_library.login
                )
            ) AS games_for_login
            WHERE games_for_login.login = gen_game_not_in_library.login
        )
        INTO games_array;
        RETURN games_array[FLOOR(random() * ARRAY_LENGTH(games_array,1)+1)::INTEGER];
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO wishlists 
(login, game_id)
    SELECT
        arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
        gen_game_not_in_library(arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1]) AS game_id
    FROM (
        SELECT generate_series(1,1000), FLOOR(random()*10000+1)::INTEGER as i
    ) AS gen
    CROSS JOIN(
        SELECT 
            ARRAY (
                SELECT login FROM users
            ) as login
    ) AS arrays
ON CONFLICT DO NOTHING;
 
INSERT INTO achievements 
(game_id, title, description, visibility)
    SELECT
        arrays.game_id[gen.i % ARRAY_LENGTH(arrays.game_id,1) + 1] AS game_id,
        MD5(random()::TEXT),
        MD5(random()::TEXT),
        random() > 0.3
    FROM (
        SELECT generate_series(1,1000), FLOOR(random()*10000+1)::INTEGER as i
    ) AS gen
    CROSS JOIN(
        SELECT 
            ARRAY (
                SELECT login FROM users
            ) as login,
            ARRAY (
                SELECT game_id FROM games
            ) as game_id
    ) AS arrays
ON CONFLICT DO NOTHING;
 
CREATE OR REPLACE FUNCTION gen_achieved(login TEXT, achievement_id UUID)
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        WITH chosen_game AS (
            SELECT game_id
            FROM achievements
            WHERE achievements.achievement_id = gen_achieved.achievement_id
        )
        SELECT MAX(purchased) FROM libraries, chosen_game
        WHERE libraries.game_id = chosen_game.game_id AND libraries.login = gen_achieved.login
        INTO max_date;

        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO achievements_users
(login, achievement_id, achieved)
    WITH pregen AS (
        SELECT DISTINCT ON (login, achievement_id)
            arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
            arrays.achievement_id[gen.j % ARRAY_LENGTH(arrays.achievement_id,1) + 1] AS achievement_id,
            gen_achieved(
                arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1],
                arrays.achievement_id[gen.j % ARRAY_LENGTH(arrays.achievement_id,1) + 1]
            ) AS achieved
        FROM (
            SELECT 
                generate_series(1,20000), 
                FLOOR(random()*9999+1)::INTEGER as i,
                FLOOR(random()*9999+1)::INTEGER as j
        ) AS gen
        CROSS JOIN(
            SELECT
                ARRAY (
                    SELECT login FROM users
                ) as login,
                ARRAY (
                    SELECT achievement_id FROM achievements
                ) as achievement_id
        ) AS arrays
    )
    SELECT login, achievement_id, achieved
    FROM pregen
    WHERE pregen.achievement_id IN (
        SELECT achievement_id
        FROM (
            (
                SELECT login, game_id
                FROM libraries
                WHERE libraries.login = pregen.login
            ) AS games_of_login
            INNER JOIN achievements
            ON achievements.game_id = games_of_login.game_id
        ) AS achievements_for_login
    )
ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_changed(login TEXT) 
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(max_date_temp) FROM (
            SELECT registered AS max_date_temp FROM users WHERE users.login = gen_changed.login
        ) as un into max_date;
        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO nicknames 
(login, nickname, changed)
    SELECT
        arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
        MD5(CONCAT(gen.i::TEXT, 'qwerty')) AS nickname,
        gen_changed(arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1])
    FROM (
        SELECT generate_series(1,1000), FLOOR(random()*10000+1)::INTEGER as i
    ) AS gen
    CROSS JOIN(
        SELECT 
            ARRAY (
                SELECT login FROM users
            ) as login
    ) AS arrays
ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_posted(login TEXT, game_id UUID)
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(purchased) FROM libraries
        WHERE libraries.game_id = gen_posted.game_id AND libraries.login = gen_posted.login
        INTO max_date;

        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO reviews
(login, game_id, posted, recommended, likes, body)
    WITH pregen AS (
        SELECT
            arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
            arrays.game_id[gen.j % ARRAY_LENGTH(arrays.game_id,1) + 1] AS game_id,
            gen_posted(
                arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1],
                arrays.game_id[gen.j % ARRAY_LENGTH(arrays.game_id,1) + 1]
            ) AS posted,
            random() > 0.3 AS recommended,
            FLOOR(random() * 50)::INTEGER AS likes,
            MD5(random()::text) AS body
        FROM (
            SELECT 
                generate_series(1,400), 
                FLOOR(random()*9999+1)::INTEGER as i,
                FLOOR(random()*9999+1)::INTEGER as j
        ) AS gen
        CROSS JOIN(
            SELECT
                ARRAY (
                    SELECT login FROM users
                ) as login,
                ARRAY (
                    SELECT game_id FROM games
                ) as game_id
        ) AS arrays
    )
    SELECT login, game_id, posted, recommended, likes, body
    FROM pregen
    WHERE pregen.game_id IN (
        SELECT game_id
        FROM (
            SELECT login, game_id
            FROM libraries
            WHERE libraries.login = pregen.login
        ) as games_for_login
    )

ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_posted(game_id UUID)
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(released) FROM games
        WHERE games.game_id = gen_posted.game_id
        INTO max_date;

        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO news
(company_id, game_id, posted, likes, body)
    WITH pregen AS (
        SELECT
            arrays.company_id[gen.i % ARRAY_LENGTH(arrays.company_id,1) + 1] AS company_id,
            arrays.game_id[gen.j % ARRAY_LENGTH(arrays.game_id,1) + 1] AS game_id,
            gen_posted(
                arrays.game_id[gen.j % ARRAY_LENGTH(arrays.game_id,1) + 1]
            ) AS posted,
            FLOOR(random() * 200)::INTEGER AS likes,
            MD5(random()::text) AS body
        FROM (
            SELECT 
                generate_series(1,1000), 
                FLOOR(random()*9999+1)::INTEGER as i,
                FLOOR(random()*9999+1)::INTEGER as j
        ) AS gen
        CROSS JOIN(
            SELECT
                ARRAY (
                    SELECT company_id FROM companies
                ) as company_id,
                ARRAY (
                    SELECT game_id FROM games
                ) as game_id
        ) AS arrays
    )
    SELECT company_id, game_id, posted, likes, body
    FROM pregen
    WHERE (
        pregen.company_id IN (
            SELECT company_id
            FROM publishers
            WHERE publishers.game_id = pregen.game_id
        )
        OR
        pregen.company_id IN (
            SELECT company_id
            FROM developers
            WHERE developers.game_id = pregen.game_id
        )
    )

ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_commented_users(sender_login TEXT, reciever_login TEXT)
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(max_date_temp) FROM (
            SELECT registered AS max_date_temp FROM users WHERE users.login = gen_commented_users.sender_login
            UNION
            SELECT registered AS max_date_temp FROM users WHERE users.login = gen_commented_users.reciever_login
        ) as un into max_date;
        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO comments_on_users
(sender_login, reciever_login, commented, body)
    WITH pregen AS (
        SELECT
            arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS sender_login,
            arrays.login[gen.j % ARRAY_LENGTH(arrays.login,1) + 1] AS reciever_login,
            gen_commented_users(
                arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1],
                arrays.login[gen.j % ARRAY_LENGTH(arrays.login,1) + 1]
            ) AS commented,
            MD5(random()::text) AS body
        FROM (
            SELECT 
                generate_series(1,1000), 
                FLOOR(random()*9999+1)::INTEGER as i,
                FLOOR(random()*9999+1)::INTEGER as j
        ) AS gen
        CROSS JOIN(
            SELECT
                ARRAY (
                    SELECT login FROM users
                ) as login
        ) AS arrays
    )
    SELECT sender_login, reciever_login, commented, body
    FROM pregen

ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_commented_reviews(login TEXT, review_id UUID)
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(max_date_temp) FROM (
            SELECT registered AS max_date_temp FROM users WHERE users.login = gen_commented_reviews.login
            UNION
            SELECT posted AS max_date_temp FROM reviews WHERE reviews.review_id = gen_commented_reviews.review_id
        ) as un into max_date;
        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO comments_on_reviews
(login, review_id, commented, body)
    WITH pregen AS (
        SELECT
            arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
            arrays.review_id[gen.j % ARRAY_LENGTH(arrays.review_id,1) + 1] AS review_id,
            gen_commented_reviews(
                arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1],
                arrays.review_id[gen.j % ARRAY_LENGTH(arrays.review_id,1) + 1]
            ) AS commented,
            MD5(random()::text) AS body
        FROM (
            SELECT 
                generate_series(1,800), 
                FLOOR(random()*9999+1)::INTEGER as i,
                FLOOR(random()*9999+1)::INTEGER as j
        ) AS gen
        CROSS JOIN(
            SELECT
                ARRAY (
                    SELECT login FROM users
                ) as login,
                ARRAY (
                    SELECT review_id FROM reviews
                ) as review_id
        ) AS arrays
    )
    SELECT login, review_id, commented, body
    FROM pregen

ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION gen_commented_news(login TEXT, news_id UUID)
    RETURNS DATE AS
    $$
    DECLARE
        max_date DATE;
    BEGIN
        SELECT MAX(max_date_temp) FROM (
            SELECT registered AS max_date_temp FROM users WHERE users.login = gen_commented_news.login
            UNION
            SELECT posted AS max_date_temp FROM news WHERE news.news_id = gen_commented_news.news_id
        ) as un into max_date;
        RETURN max_date + FLOOR(random() * (CURRENT_DATE - max_date))::INTEGER;
    END;
    $$
    LANGUAGE plpgsql;

INSERT INTO comments_on_news
(login, news_id, commented, body)
    WITH pregen AS (
        SELECT
            arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1] AS login,
            arrays.news_id[gen.j % ARRAY_LENGTH(arrays.news_id,1) + 1] AS news_id,
            gen_commented_news(
                arrays.login[gen.i % ARRAY_LENGTH(arrays.login,1) + 1],
                arrays.news_id[gen.j % ARRAY_LENGTH(arrays.news_id,1) + 1]
            ) AS commented,
            MD5(random()::text) AS body
        FROM (
            SELECT 
                generate_series(1,800), 
                FLOOR(random()*9999+1)::INTEGER as i,
                FLOOR(random()*9999+1)::INTEGER as j
        ) AS gen
        CROSS JOIN(
            SELECT
                ARRAY (
                    SELECT login FROM users
                ) as login,
                ARRAY (
                    SELECT news_id FROM news
                ) as news_id
        ) AS arrays
    )
    SELECT login, news_id, commented, body
    FROM pregen

ON CONFLICT DO NOTHING;

