CREATE EXTENSION IF NOT EXISTS pldbgapi;
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS countries (
    country TEXT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS users (
    login TEXT PRIMARY KEY,
    pswd TEXT NOT NULL,
    email TEXT,
    registered DATE NOT NULL DEFAULT CURRENT_DATE,
    country TEXT REFERENCES countries
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    info TEXT
);

CREATE TABLE IF NOT EXISTS nicknames (
    nickname_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    nickname TEXT NOT NULL,
    changed DATE NOT NULL DEFAULT CURRENT_DATE 
);

CREATE TABLE IF NOT EXISTS games (
    game_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    title TEXT NOT NULL,
    game_series TEXT,
    released DATE NOT NULL DEFAULT CURRENT_DATE,
    abstract TEXT NOT NULL,
    description TEXT NOT NULL, 
    requirements TEXT NOT NULL,
    price MONEY NOT NULL DEFAULT 0
        CHECK(price >= CAST (0 as MONEY))
);

CREATE TABLE IF NOT EXISTS genres (
    genre TEXT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS genres_games (
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    genre TEXT REFERENCES genres
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    PRIMARY KEY (game_id, genre)
);

CREATE TABLE IF NOT EXISTS tags (
    tag TEXT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS tags_games (
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    tag TEXT REFERENCES tags
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    PRIMARY KEY (game_id, tag)
);

CREATE TABLE IF NOT EXISTS libraries (
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    in_game_time INTERVAL NOT NULL DEFAULT '0 SECOND',
    bill TEXT NOT NULL,
    purchased DATE NOT NULL DEFAULT CURRENT_DATE,
    PRIMARY KEY (game_id, login)
);

CREATE FUNCTION libraries_check() 
    RETURNS TRIGGER AS
    $$
    DECLARE
        registered DATE;
        released DATE;
    BEGIN
        SELECT MAX(users.registered) INTO registered FROM users WHERE login=NEW.login;
        SELECT MAX(games.released) INTO released FROM games WHERE game_id=NEW.game_id;
        IF NEW.purchased < registered THEN
            RAISE EXCEPTION 'Cannot be purchased before registration';
        END IF;
        IF NEW.purchased < released THEN
            RAISE EXCEPTION 'Cannot be purchased before release';
        END IF;

        RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;

CREATE TRIGGER libraries_check  
    BEFORE INSERT OR UPDATE ON libraries
    FOR EACH ROW 
    EXECUTE PROCEDURE libraries_check();


CREATE TABLE IF NOT EXISTS wishlists (
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    PRIMARY KEY (game_id, login)
);

CREATE TABLE IF NOT EXISTS achievements (
    achievement_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    visibility BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS achievements_users (
    achievement_id UUID REFERENCES achievements
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    achieved DATE NOT NULL DEFAULT CURRENT_DATE,
    PRIMARY KEY (achievement_id, login)
);

CREATE FUNCTION achievements_users_check() 
    RETURNS TRIGGER AS
    $$
    DECLARE
        purchased DATE;
        game UUID;
    BEGIN
        SELECT game_id 
        FROM achievements 
        WHERE NEW.achievement_id = achievements.achievement_id
        INTO game;

        SELECT MAX(libraries.purchased) INTO purchased FROM libraries 
        WHERE login=NEW.login
        AND game_id=game;

        IF purchased IS NULL THEN
            RAISE EXCEPTION 'Cannot be achieved before purchase (NULL)';
        END IF;
        IF NEW.achieved < purchased THEN
            RAISE EXCEPTION 'Cannot be achieved before purchase';
        END IF;

        RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;

CREATE TRIGGER achievements_users_check  
    BEFORE INSERT OR UPDATE ON achievements_users
    FOR EACH ROW 
    EXECUTE PROCEDURE achievements_users_check();

CREATE TABLE IF NOT EXISTS reviews (
    review_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    posted DATE NOT NULL DEFAULT CURRENT_DATE,
    recommended BOOLEAN NOT NULL DEFAULT TRUE, 
    likes INTEGER NOT NULL DEFAULT 0
        CHECK(likes >= 0),
    body TEXT NOT NULL
);

CREATE FUNCTION reviews_check() 
    RETURNS TRIGGER AS
    $$
    DECLARE
        purchased DATE;
        game UUID;
    BEGIN
        SELECT MAX(libraries.purchased) INTO purchased FROM libraries 
        WHERE login=NEW.login
        AND game_id=NEW.game_id;

        IF purchased IS NULL THEN
            RAISE EXCEPTION 'Reviews cannot be posted before purchase (NULL)';
        END IF;
        IF NEW.posted < purchased THEN
            RAISE EXCEPTION 'Reviews cannot be posted before purchase';
        END IF;

        RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;

CREATE TRIGGER reviews_check  
    BEFORE INSERT OR UPDATE ON reviews
    FOR EACH ROW 
        EXECUTE PROCEDURE reviews_check();

CREATE TABLE IF NOT EXISTS companies (
    company_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    title TEXT NOT NULL,
    info TEXT
);

CREATE TABLE IF NOT EXISTS developers (
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    company_id UUID REFERENCES companies
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    PRIMARY KEY (game_id, company_id)
);

CREATE TABLE IF NOT EXISTS publishers (
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    company_id UUID REFERENCES companies
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    PRIMARY KEY (game_id, company_id)
);

CREATE TABLE IF NOT EXISTS news (
    news_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    company_id UUID REFERENCES companies
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    game_id UUID REFERENCES games
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    posted DATE NOT NULL DEFAULT CURRENT_DATE,
    likes INTEGER NOT NULL DEFAULT 0
        CHECK(likes >= 0),
    body TEXT NOT NULL
);

CREATE FUNCTION news_check() 
    RETURNS TRIGGER AS
    $$
    DECLARE
        released DATE;
        publisher_id UUID;
        developer_id UUID;
    BEGIN
        SELECT MAX(games.released) INTO released FROM games 
        WHERE game_id=NEW.game_id;

        SELECT company_id
        FROM publishers
        WHERE publishers.game_id = NEW.game_id
        INTO publisher_id;

        SELECT company_id
        FROM developers
        WHERE developers.game_id = NEW.game_id
        INTO developer_id;

    IF released IS NULL THEN
        RAISE EXCEPTION 'News cannot be posted before release (NULL)';
    END IF;
    IF NEW.company_id != developer_id AND NEW.company_id != publisher_id THEN
        RAISE EXCEPTION 'News cannot be posted for the game of other company';
    END IF;
    IF NEW.posted < released THEN
        RAISE EXCEPTION 'News cannot be posted before release';
    END IF;

    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER news_check  
    BEFORE INSERT OR UPDATE ON news
    FOR EACH ROW 
        EXECUTE PROCEDURE news_check();

CREATE TABLE IF NOT EXISTS comments_on_users (
    comment_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    sender_login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    reciever_login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    commented DATE NOT NULL DEFAULT CURRENT_DATE,
    body TEXT NOT NULL
);

CREATE FUNCTION comments_on_users_check() 
    RETURNS TRIGGER AS
    $$
    DECLARE
        sended DATE;
        recieved DATE;
    BEGIN
        SELECT MAX(users.registered) INTO sended FROM users WHERE login=NEW.sender_login;
        SELECT MAX(users.registered) INTO recieved FROM users WHERE login=NEW.reciever_login;
        IF NEW.commented < sended THEN
            RAISE EXCEPTION 'Cannot comment before registration';
        END IF;
        IF NEW.commented < recieved THEN
            RAISE EXCEPTION 'Cannot commet to unregistered user';
        END IF;

        RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;

CREATE TRIGGER comments_on_users_check  
BEFORE INSERT OR UPDATE ON comments_on_users
FOR EACH ROW 
    EXECUTE PROCEDURE comments_on_users_check();

CREATE TABLE IF NOT EXISTS comments_on_reviews (
    comment_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    review_id UUID REFERENCES reviews
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    commented DATE NOT NULL DEFAULT CURRENT_DATE,
    body TEXT NOT NULL
);

CREATE FUNCTION comments_on_reviews_check() 
    RETURNS TRIGGER AS
    $$
    DECLARE
        registered DATE;
        posted DATE;
    BEGIN
        SELECT MAX(users.registered) INTO registered FROM users WHERE login=NEW.login;
        SELECT MAX(reviews.posted) INTO posted FROM reviews WHERE review_id=NEW.review_id;
        IF NEW.commented < registered THEN
            RAISE EXCEPTION 'Cannot comment before registration';
        END IF;
        IF NEW.commented < posted THEN
            RAISE EXCEPTION 'Cannot commet to unexist review';
        END IF;

        RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;

CREATE TRIGGER comments_on_reviews_check  
BEFORE INSERT OR UPDATE ON comments_on_reviews
FOR EACH ROW 
    EXECUTE PROCEDURE comments_on_reviews_check();

CREATE TABLE IF NOT EXISTS comments_on_news (
    comment_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    login TEXT REFERENCES users
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    news_id UUID REFERENCES news
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    commented DATE NOT NULL DEFAULT CURRENT_DATE,
    body TEXT NOT NULL
);
